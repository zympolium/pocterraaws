# Region is specified with environment variable cf. .gitlab-ci.yml

# The VM
resource "aws_instance" "myVM" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
    host = "${self.public_ip}"
    # The connection will use the local SSH agent for authentication.
  }
  
  instance_type = "${var.instance_type}"

  # From market place : https://aws.amazon.com/marketplace/
  # -> https://aws.amazon.com/marketplace/pp/B07CQ33QKV
  # -> https://gist.github.com/magnetikonline/e822a78a9b691d86d6e45626f8f0c977
  ami = "ami-08b314ce48a790a19"

  # The name of our SSH keypair we created in access.tf
  key_name = "${aws_key_pair.deployer.id}"

  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.default.id}"]

  # We're going to launch into the same subnet as our ELB. In a production
  # environment it's more common to have a separate private subnet for
  # backend instances.
  subnet_id = "${aws_subnet.default.id}"
}
