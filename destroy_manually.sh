#!/bin/bash

# -- Config
. ./config/set_env.sh

# -- Constantes
public_key_path=~/.ssh/terraform.pub
key_name=deployer
echo "$SSH_PUBLIC_KEY" > $public_key_path

# -- Main
terraform destroy \
    -auto-approve \
    -var "key_name=$key_name" \
    -var "public_key_path=$public_key_path" \
    terraform