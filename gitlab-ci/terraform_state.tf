# state.tf
terraform {
  backend "s3" {
    bucket = "manage-terraform-state"
    key    = "terraform-state.tfstate"
    region = "us-east-1"
  }
}
