Test provisionning a VM on AWS with Terraform

# Manually

### Prerequisite (Ubuntu by example)
  * Terraform installation : https://computingforgeeks.com/how-to-install-terraform-on-ubuntu-centos-7/
  * aws cli installation : `sudo apt install awscli`
  * Clone this repository

### Configuration
  * Copy template settings file : ```cp config/set_env.example.sh config/set_env.sh```
  * Set values for all variables in `config/set_env.sh`, if you need some help cf. "GitLab configuration" below

Note
  * If you want AWS Free Usage Tier, specify `t2.micro` in `terraform/variables.tf`
  * If you change default zone you may need to change `ami` value in `terraform/VM.tf`

### Do
  * Create infrastructure : `./create_manually.sh`
  * Destroy infrastructure : `./destroy_manually.sh`

### Connect to your instance (VM)
  * Assume that `~/.ssh/terraform` is the private key : public key is set in DEPLOYER_SSH_KEY
  * Get the public IP last line stdout Terraform apply plan : `ssh -i ~/.ssh/terraform ubuntu@<public-ip>`
  * Just run `sudo apt update -q && sudo apt install -y python-pip` if you want to install Python dependencies with pip

# Manage with GitLab CI

### HOW TO
  * First configure your project as describe next step
  * Default push on master will create / update the infrastructure as describe in terraform directory
  * The pipeline will get a manual job to destroy the infrastructure

### GitLab configuration
You just need to set your security informations in GitLab to have a successful pipeline

Variables can be found in `Settings -> CI/CD - > Variables`

  * **AWS_ACCESS_KEY_ID**
    * Create a new account in AWS : IAM with role :
      * AmazonEC2FullAccess
      * AmazonS3FullAccess
      * AmazonDynamoDBFullAccess
      * AmazonRDSFullAccess
      * CloudWatchFullAccess
      * IAMFullAccess
    * Create a new access key
      * Get the Access key ID in Security Credentials
  * **AWS_SECRET_ACCESS_KEY**
    * Get the value from precedent access key creation
  * **AWS_DEFAULT_REGION**
    * Choose the zone you want : https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
  * **DEPLOYER_SSH_KEY**
    * You need to create an SSH key to manage access to the VM, the VM will be configured to authorize your connection with you public key
    * Execute from your access device and copy results from last command
```
ssh-keygen -f ~/.ssh/terraform
cat ~/.ssh/terraform.pub
```
## Terraform state manage with backend through S3 bucket
For successful GitLab CI chaining pipelines you need an S3 bucket to store Tearraform plan and state, otherwise it will recreate new objets, just create the bucket and fill in `gitlab-ci/terraform_state.tf` (https://www.terraform.io/docs/backends/types/s3.html)

## AWS specificity
  * Accepting the ami terms on the marketplace for the ami you want : clic on the product, accept 
  * Example : https://aws.amazon.com/marketplace/pp/B07CQ33QKV

# Bibliography

## Terraform
  * https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180#a9b0
  * https://github.com/terraform-providers/terraform-provider-aws/blob/master/examples/two-tier/main.tf

## GitLab CI
  * https://medium.com/@timhberry/terraform-pipelines-in-gitlab-415b9d842596 
  * https://www.nvisia.com/insights/terraform-automation-with-gitlab-aws
  * https://github.com/defo89/hello-world-gitlab-ci